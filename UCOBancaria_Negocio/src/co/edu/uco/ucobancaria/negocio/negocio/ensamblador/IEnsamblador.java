package co.edu.uco.ucobancaria.negocio.negocio.ensamblador;

import java.util.List;

import co.edu.uco.ucobase.utilitarios.dominio.enumeracion.OperacionEnum;

public interface IEnsamblador<D, T> {

	T enmsablarDTO(D objetoDominio);

	D ensamblarDominio(T objetoDto, OperacionEnum operacion);

	List<T> enmsablarListaDTO(List<D> listaObjetosDominio);
}
